/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';
/**
 * Write the unit tests for your transction processor functions here
 */

const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const { BusinessNetworkDefinition, CertificateUtil, IdCard } = require('composer-common');
const path = require('path');

const chai = require('chai');
chai.should();
chai.use(require('chai-as-promised'));

const namespace = 'org.acme.bmr';
// assets
const patientInfoType = 'healthdata.PatientInformation';
const patientInfoNS = namespace + '.' + patientInfoType;
// participants
const doctorType = 'participants.Doctor';
const doctorNS = namespace + '.' + doctorType;
const patientType = 'participants.Patient';
const patientNS = namespace + '.' + patientType;
const aliceId = 'alice@email.com';
const bobId = 'bob@email.com';
const patrickId = 'patrick@email.com';
const pamelaId = 'pamela@email.com';
// Transactions
const participantNamespace = 'org.acme.bmr.participants';
const healthdataNamespace = 'org.acme.bmr.healthdata';
const UpdateMedicationType = 'UpdateMedication';

describe('#' + namespace, () => {
    // In-memory card store for testing so cards are not persisted to the file system
    const cardStore = require('composer-common').NetworkCardStoreManager.getCardStore( { type: 'composer-wallet-inmemory' } );

    // Embedded connection used for local testing
    const connectionProfile = {
        name: 'embedded',
        'x-type': 'embedded'
    };

    // Name of the business network card containing the administrative identity for the business network
    const adminCardName = 'admin';

    // Admin connection to the blockchain, used to deploy the business network
    let adminConnection;

    // This is the business network connection the tests will use.
    let businessNetworkConnection;

    // This is the factory for creating instances of types.
    let factory;

    // These are the identities for Alice and Bob.
    const doctorAliceCardName = 'alice';
    const doctorBobCardName = 'bob';

    // These are a list of receieved events.
    let events;

    let businessNetworkName;

    before(async () => {
        // Generate certificates for use with the embedded connection
        const credentials = CertificateUtil.generate({ commonName: 'admin' });

        // Identity used with the admin connection to deploy business networks
        const deployerMetadata = {
            version: 1,
            userName: 'PeerAdmin',
            roles: [ 'PeerAdmin', 'ChannelAdmin' ]
        };
        const deployerCard = new IdCard(deployerMetadata, connectionProfile);
        deployerCard.setCredentials(credentials);
        const deployerCardName = 'PeerAdmin';

        adminConnection = new AdminConnection({ cardStore: cardStore });

        await adminConnection.importCard(deployerCardName, deployerCard);
        await adminConnection.connect(deployerCardName);
    });

    /**
     *
     * @param {String} cardName The card name to use for this identity
     * @param {Object} identity The identity details
     */
    async function importCardForIdentity(cardName, identity) {
        const metadata = {
            userName: identity.userID,
            version: 1,
            enrollmentSecret: identity.userSecret,
            businessNetwork: businessNetworkName
        };
        const card = new IdCard(metadata, connectionProfile);
        await adminConnection.importCard(cardName, card);
    }

    // This is called before each test is executed.
    beforeEach(async () => {
        // Generate a business network definition from the project directory.
        let businessNetworkDefinition = await BusinessNetworkDefinition.fromDirectory(path.resolve(__dirname, '..'));
        businessNetworkName = businessNetworkDefinition.getName();
        await adminConnection.install(businessNetworkDefinition);
        const startOptions = {
            networkAdmins: [
                {
                    userName: 'admin',
                    enrollmentSecret: 'adminpw'
                }
            ]
        };
        const adminCards = await adminConnection.start(businessNetworkName, businessNetworkDefinition.getVersion(), startOptions);
        await adminConnection.importCard(adminCardName, adminCards.get('admin'));

        // Create and establish a business network connection
        businessNetworkConnection = new BusinessNetworkConnection({ cardStore: cardStore });
        events = [];
        businessNetworkConnection.on('event', event => {
            events.push(event);
        });
        await businessNetworkConnection.connect(adminCardName);

        // Get the factory for the business network.
        factory = businessNetworkConnection.getBusinessNetwork().getFactory();

        const doctorRegistry = await businessNetworkConnection.getParticipantRegistry(doctorNS);
        const patientRegistry = await businessNetworkConnection.getParticipantRegistry(patientNS);

        // Create the participants.
        const alice = factory.newResource(namespace, doctorType, aliceId);
        alice.firstName = 'Alice';
        alice.lastName = 'D';
        alice.hospital = 'Yale Health';
        alice.specialty = 'PRIMARY_CARE';

        const bob = factory.newResource(namespace, doctorType, bobId);
        bob.firstName = 'Bob';
        bob.lastName = 'D';
        bob.hospital = 'Yale Health';
        bob.specialty = 'PEDIATRICIAN';

        doctorRegistry.addAll([alice, bob]);

        const patient1 = factory.newResource(namespace, patientType, patrickId);
        patient1.firstName = 'Patrick';
        patient1.lastName = 'P';
        patient1.information = factory.newRelationship(namespace, patientInfoType, patrickId);

        const patient2 = factory.newResource(namespace, patientType, 'pamela@email.com');
        patient2.firstName = 'Pamela';
        patient2.lastName = 'P';
        patient2.information = factory.newRelationship(namespace, patientInfoType, 'pamela@email.com');

        const assetRegistry = await businessNetworkConnection.getAssetRegistry(patientInfoNS);
        // Create the assets.
        const info1 = factory.newResource(namespace, patientInfoType, patrickId);
        info1.owner = factory.newRelationship(namespace, doctorType, aliceId);
        info1.patient = factory.newRelationship(namespace, patientType, patrickId);
        info1.medicationList = [];
        info1.diagnosisList = [];

        const info2 = factory.newResource(namespace, patientInfoType, 'pamela@email.com');
        info2.owner = factory.newRelationship(namespace, doctorType, bobId);
        info2.patient = factory.newRelationship(namespace, patientType, 'pamela@email.com');
        info2.medicationList = [];
        info2.diagnosisList = [];

        patientRegistry.addAll([patient1, patient2]);
        assetRegistry.addAll([info1, info2]);

        // Issue the identities.
        let identity = await businessNetworkConnection.issueIdentity(doctorNS + '#alice@email.com', 'alice1');
        await importCardForIdentity(doctorAliceCardName, identity);
        identity = await businessNetworkConnection.issueIdentity(doctorNS + '#bob@email.com', 'bob1');
        await importCardForIdentity(doctorBobCardName, identity);
    });

    /**
     * Reconnect using a different identity.
     * @param {String} cardName The name of the card for the identity to use
     */
    async function useIdentity(cardName) {
        await businessNetworkConnection.disconnect();
        businessNetworkConnection = new BusinessNetworkConnection({ cardStore: cardStore });
        events = [];
        businessNetworkConnection.on('event', (event) => {
            events.push(event);
        });
        await businessNetworkConnection.connect(cardName);
        factory = businessNetworkConnection.getBusinessNetwork().getFactory();
    }

    it('Alice can only read patient1\'s information', async () => {
        // Use the identity for Alice.
        await useIdentity(doctorAliceCardName);
        const assetRegistry = await businessNetworkConnection.getAssetRegistry(patientInfoNS);
        const assets = await assetRegistry.getAll();

        // Alice can only see Patrick but not Pamela
        assets.should.have.lengthOf(1);
        const asset1 = assets[0];
        asset1.owner.getFullyQualifiedIdentifier().should.equal(doctorNS + '#alice@email.com');
        asset1.patientId.should.equal(patrickId);
        asset1.patient.getFullyQualifiedIdentifier().should.equal(patientNS + '#patrick@email.com');
    });

    it('Bob can only read patient2\'s information', async () => {
        // Use the identity for Bob.
        await useIdentity(doctorBobCardName);
        const assetRegistry = await businessNetworkConnection.getAssetRegistry(patientInfoNS);
        const assets = await assetRegistry.getAll();

        // Bob can only see Pamela but not Patrick
        assets.should.have.lengthOf(1);
        const asset2 = assets[0];
        asset2.owner.getFullyQualifiedIdentifier().should.equal(doctorNS + '#bob@email.com');
        asset2.patientId.should.equal('pamela@email.com');
        asset2.patient.getFullyQualifiedIdentifier().should.equal(patientNS + '#pamela@email.com');
    });

    it('Alice cannot add patient information even if she owns it', async () => {
        // Use the identity for Alice.
        await useIdentity(doctorAliceCardName);
        const newPatientId = 'paul@email.com';

        // Create the asset.
        let asset3 = factory.newResource(namespace, patientInfoType, newPatientId);
        asset3.owner   = factory.newRelationship(namespace, doctorType, aliceId);
        asset3.patient = factory.newRelationship(namespace, patientType, newPatientId);
        asset3.medicationList = [];
        asset3.diagnosisList = [];

        // Try to add the asset
        const assetRegistry = await businessNetworkConnection.getAssetRegistry(patientInfoNS);
        assetRegistry.add(asset3).should.be.rejectedWith(/does not have .* access to resource/);
    });

    it('Alice cannot update her patient information normally', async () => {
        // Use the identity for Alice.
        await useIdentity(doctorAliceCardName);
        const assetRegistry = await businessNetworkConnection.getAssetRegistry(patientInfoNS);
        const assets = await assetRegistry.getAll();

        // Alice can only see Patrick but not Pamela
        assets.should.have.lengthOf(1);
        const asset1 = assets[0];
        asset1.patient.getFullyQualifiedIdentifier().should.equal(patientNS + '#patrick@email.com');

        // Change asset
        asset1.medicationList = ['malicious', 'update'];
        asset1.patient = factory.newRelationship(namespace, patientType, 'try to change patient name');

        // Try to update asset
        assetRegistry.update(asset1).should.be.rejectedWith(/does not have .* access to resource/);
    });

    it('Alice can update her patient information through transaction', async () => {
        // Use the identity for Alice.
        await console.log('start');
        await useIdentity(doctorAliceCardName);
        const medication = 'This is new medication of Patrick';

        // Submit the transaction.
        const transaction = factory.newTransaction(participantNamespace, UpdateMedicationType);
        transaction.information = factory.newRelationship(namespace, patientInfoType, patrickId);
        transaction.newMedication = medication;
        await businessNetworkConnection.submitTransaction(transaction);

        // // Get the asset.
        const assetRegistry = await businessNetworkConnection.getAssetRegistry(patientInfoNS);
        const patrickInfo = await assetRegistry.get(patrickId);

        // Validate the asset.
        patrickInfo.owner.getFullyQualifiedIdentifier().should.equal(doctorNS + '#' + aliceId);
        patrickInfo.medicationList.should.deep.equal([medication]);

        // Validate the events.
        events.should.have.lengthOf(0);
    });

});
