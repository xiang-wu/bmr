/* 
 * Model patient health data. 
 */

 namespace org.acme.bmr.healthdata
 import org.acme.bmr.participants.Patient
 import org.acme.bmr.participants.Doctor
 
/* Medical information */
asset PatientInformation identified by patientId {
  o String patientId 
  --> Patient patient 
  --> Doctor owner 
  --> Doctor primaryDoctor optional
  o String[] medicationList
  o String[] diagnosisList
}

transaction updateMedication {
  --> PatientInformation patientInfo 
  o String newMedication
}

transaction updateDiagnosisList {
  --> PatientInformation patientInfo 
  o String newDiagnosis
}

/* Diagnosis */
concept Diagnosis{
  o String diagnosisName
  o DateTime diagnosisStartDate
  o DateTime diagnosisEndDate optional
}

/* Medication */
concept PrescriptionMeds{
  o String medName
  o String genericMedName optional
  o Double medDose
  o String medDoseUnits
  o Diagnosis primaryDiagonsis
  o DateTime medStartDate
  o DateTime medEndDate optional
}

/* Lab Information */
enum LabNames {
  o CPC
  o RBC 
  o WBC
  o HGB 
  o HCT
  o PLT
  o A1C
  o GLU
  o CHOL
}

concept LaboratoryTests{
  o String labName
  o Double labResultValue
  o DateTime labDate
}
