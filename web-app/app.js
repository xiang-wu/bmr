var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// Routes
var indexRouter = require('./routes/index');

var patientLoginRouter = require('./routes/patientLogin');
var patientRegisterRouter = require('./routes/patientRegister');
var patientHomeRouter = require('./routes/patientHome');

var doctorHomeRouter = require('./routes/doctorHome');
var doctorRegisterRouter = require('./routes/doctorRegister');
var doctorLoginRouter = require('./routes/doctorLogin');
var doctorUpdateMedListRouter = require('./routes/updateMedList');

var app = express();

//get the libraries to call
var network = require('./network/network');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Home indexes 
app.use('/', indexRouter);
app.use('/doctorHome', doctorHomeRouter);
app.use('/patientHome', patientHomeRouter);

// Patient Routes
app.use('/patientLogin', patientLoginRouter);
app.post('/patientLogin',function(req, res) {
  //declare variables to retrieve from request
  var cardId = req.body.cardId;
  var patientId = req.body.patientId;

  var returnData = {};

 network.patientData(cardId, patientId)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        var firstName = response.firstName;
        var lastName = response.lastName;

        network.patientHealthData(cardId, patientId)
          .then((response) => {
            if (response.error != null) {
              res.json({
                error: response.error
              });
            } else {
              console.log(response.medicationList);
              var medList = response.medicationList;

              metadata = {
                firstName: firstName,
                lastName:lastName,
                medList: medList
              };

              res.render('patient/patientHome', metadata);
            }
          });
      }
    });
});

app.use('/patientRegister', patientRegisterRouter);
app.post('/patientRegister',function(req, res) {
  console.log(req.body)
  
  //declare variables to retrieve from request
  var cardId = req.body.cardId;
  var firstName = req.body.firstName;
  var lastName = req.body.lastName;

  network.registerPatient(cardId, firstName, lastName)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        return res.redirect('/patientLogin');
      }
    });
});


// Doctor Routes
app.use('/doctorRegister', doctorRegisterRouter);
app.post('/doctorRegister', function(req, res) {
  console.log(req.body);

  var cardId = req.body.cardId;
  var firstName = req.body.firstName;
  var lastName = req.body.lastName;
  var institution = req.body.institution;
  var specialty = req.body.specialty

  network.registerDoctor(cardId, firstName, lastName, institution, specialty)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        return res.redirect('/doctorLogin');
      }
    });
});

app.use('/doctorLogin', doctorLoginRouter);
app.post('/doctorLogin',function(req, res) {
  //declare variables to retrieve from request
  var cardId = req.body.cardId;
  var doctorId = req.body.doctorId;

  network.doctorData(cardId, doctorId)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        return res.redirect('/doctorHome');
      }
    });
});

app.use('/updateMedList', doctorUpdateMedListRouter);
app.post('/updateMedList', function(req, res) {
  console.log("Ready to update!", req.body);

  var doctorId = req.body.doctorId;
  var patientId = req.body.patientId;
  var newMed = req.body.newMed;
  network.updateMedicationList(doctorId, patientId, newMed)
    .then((response) => {
      if (response.error != null) {
        res.json({
          error: response.error
        });
      } else {
        res.send('Added new medication!');
      }
    });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


//declare port
var port = process.env.PORT || 8000;
if (process.env.VCAP_APPLICATION) {
  port = process.env.PORT;
}

//run app on port
app.listen(port, function() {
  console.log('app running on port: %d', port);
});


module.exports = app;
