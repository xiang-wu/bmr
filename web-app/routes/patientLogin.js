var express = require('express');
var router = express.Router();

/* GET patient login page. */
router.get('/', function(req, res, next) {
  console.log("Patient Log In Route.")
  res.render('patient/patientLogin');
});

module.exports = router;
