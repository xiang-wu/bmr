var express = require('express');
var router = express.Router();

/* GET patient login page. */
router.get('/', function(req, res, next) {
  console.log("Patient Home Route.", req.body);
  res.render('patient/patientHome');
});

module.exports = router;
