var express = require('express');
var router = express.Router();

/* GET patient login page. */
router.get('/', function(req, res, next) {
  console.log("Doctor Home Route.")
  res.render('doctor/doctorHome');
});

module.exports = router;
