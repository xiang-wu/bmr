var express = require('express');
var router = express.Router();

/* GET patient login page. */
router.get('/', function(req, res, next) {
  console.log("Doctor Register Route.")
  res.render('doctor/doctorRegister');
});

module.exports = router;
