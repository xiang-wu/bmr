var express = require('express');
var router = express.Router();

/* GET patient login page. */
router.get('/', function(req, res, next) {
  console.log("Register Patient Page")
  res.render('patient/patientRegister');
});

module.exports = router;
