var express = require('express');
var router = express.Router();

/* GET patient login page. */
router.get('/', function(req, res, next) {
  console.log("Doctor Login Route.")
  res.render('doctor/doctorLogin');
});

module.exports = router;
