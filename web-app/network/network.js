const AdminConnection = require('composer-admin').AdminConnection;
const BusinessNetworkConnection = require('composer-client').BusinessNetworkConnection;
const { BusinessNetworkDefinition, CertificateUtil, IdCard } = require('composer-common');

// Used to generate id.
var uuid = require('uuid');

// Namespaces.
let participantNamespace = 'org.acme.bmr.participants'
let healthDataNamespace = 'org.acme.bmr.healthdata'

//admin connection to the blockchain, used to deploy the business network
let adminConnection;

//this is the business network connection the tests will use.
let businessNetworkConnection;

let businessNetworkName = 'blockchain-medical-records';
let factory;

/*
 * Import card for an identity
 * @param {String} cardName The card name to use for this identity
 * @param {Object} identity The identity details
 */
async function importCardForIdentity(cardName, identity) {
    //use admin connection
    adminConnection = new AdminConnection();
  
    //declare metadata
    const metadata = {
        userName: identity.userID,
        version: 1,
        enrollmentSecret: identity.userSecret,
        businessNetwork: businessNetworkName
    };
  
    //get connectionProfile from json, create Idcard
    const connectionProfile = require('../../DevServer_connection.json');
    const card = new IdCard(metadata, connectionProfile);
  
    //import card
    await adminConnection.importCard(cardName, card);
}

 
module.exports = {
 /*
  * Create Patient participant and import card for identity
  * @param {String} cardId Import card id for member
  * @param {String} accountNumber Member account number as identifier on network
  * @param {String} firstName Member first name
  * @param {String} lastName Member last name
  */
 registerPatient: async function (cardId, firstName, lastName) {
    // Connect as administrator.
    businessNetworkConnection = new BusinessNetworkConnection();
    await businessNetworkConnection.connect('admin@blockchain-medical-records');
    
    // Get the factory for the business network.
    factory = businessNetworkConnection.getBusinessNetwork().getFactory();

    // Create member participant.
    const patientId = uuid.v1();
    console.log("patientId: ", patientId);
    const patient = factory.newResource(participantNamespace, 'Patient', patientId);
    patient.id = patientId;

    // Patient information. 
    patientInfoAsset = factory.newResource(healthDataNamespace, 'PatientInformation', cardId);
    patientInfoAsset.patientId = patientId;
    patientInfoAsset.patient = factory.newRelationship(participantNamespace, 'Patient', patientId);
    patientInfoAsset.medicationList = [];
    patientInfoAsset.diagnosisList = [];

    patientInfo = factory.newRelationship(healthDataNamespace, 'PatientInformation', cardId);
    patientInfo.patientId = patientId;
    patientInfo.patient = patient;
    patientInfo.medicationList = [];
    patientInfo.diagnosisList = [];
    patient.information = patientInfo;
    console.log("Patient info: ", patientInfo);

    patient.firstName = firstName;
    patient.lastName = lastName;
    
    // Add member participant.
    const participantRegistry = await businessNetworkConnection.getParticipantRegistry(participantNamespace +  '.Patient');
    await participantRegistry.add(patient);

    // Add patient information.
    const patientInfoRegistry = await businessNetworkConnection.getAssetRegistry(healthDataNamespace +  '.PatientInformation');
    await patientInfoRegistry.add(patientInfoAsset);

    //issue identity
    const identity = await businessNetworkConnection.issueIdentity(participantNamespace + '.Patient#' + patientId, cardId);

    //import card for identity
    await importCardForIdentity(cardId, identity);

    //disconnect
    await businessNetworkConnection.disconnect('admin@blockchain-medical-records');

    return true;
 },


 /*
  * Create Doctor participant and import card for identity
  * @param {String} cardId Import card id for member
  * @param {String} accountNumber Member account number as identifier on network
  * @param {String} firstName Member first name
  * @param {String} lastName Member last name
  */
 registerDoctor: async function (cardId, firstName, lastName, institution, specialty) {
  // Connect as administrator.
  businessNetworkConnection = new BusinessNetworkConnection();
  await businessNetworkConnection.connect('admin@blockchain-medical-records');
  
  // Get the factory for the business network.
  factory = businessNetworkConnection.getBusinessNetwork().getFactory();

  // Create doctor participant.
  const doctorId = uuid.v1();
  const doctor = factory.newResource(participantNamespace, 'Doctor', doctorId);
  doctor.doctorId= doctorId;
  doctor.firstName = firstName;
  doctor.lastName = lastName;
  doctor.hospital = institution;
  doctor.specialty = specialty;

  console.log(doctorId);

  // Add doctor participant.
  const doctorRegistry = await businessNetworkConnection.getParticipantRegistry(participantNamespace +  '.Doctor');
  await doctorRegistry.add(doctor);

  //issue identity
  const identity = await businessNetworkConnection.issueIdentity(participantNamespace + '.Doctor#' + doctorId, cardId);

  //import card for identity
  await importCardForIdentity(cardId, identity);

  //disconnect
  await businessNetworkConnection.disconnect('admin@blockchain-medical-records');

  return true;
},

  /*
  * Get Patient data
  * @param {String} cardId Card id to connect to network
  * @param {String} accountNumber Account number of member
  */
 patientData: async function (cardId, patientId) {
    try {
      // Connect to network with cardId.
      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect(cardId);

      // Get patient from the network.
      const patientRegistry = await businessNetworkConnection.getParticipantRegistry(participantNamespace + '.Patient');
      const patient = await patientRegistry.get(patientId);

      // Disconnect.
      await businessNetworkConnection.disconnect(cardId);

      // Return patient object
      return patient;
    }
    catch(err) {
      //print and return error
      console.log("Error", err);
      var error = {};
      error.error = err.message;
      return error;
    }
  },

  /*
  * Get Patient health data
  * @param {String} cardId Card id to connect to network
  * @param {String} accountNumber Account number of member
  */
 patientHealthData: async function (cardId, patientId) {
  try {
    // Connect to network with cardId.
    businessNetworkConnection = new BusinessNetworkConnection();
    await businessNetworkConnection.connect(cardId);

    // Get patient from the network.
    const patientInfoRegistry = await businessNetworkConnection.getAssetRegistry(healthDataNamespace + '.PatientInformation');
    // Patient info identifier is the cardId
    const patientInfo = await patientInfoRegistry.get(patientId);

    // Disconnect.
    await businessNetworkConnection.disconnect(cardId);

    // Return patient object
    return patientInfo;
  }
  catch(err) {
    //print and return error
    console.log("Error", err);
    var error = {};
    error.error = err.message;
    return error;
  }
},

   /*
  * Get Doctor data
  * @param {String} cardId Card id to connect to network
  * @param {String} accountNumber Account number of member
  */
 doctorData: async function (cardId, doctorId) {
    try {
      // Connect to network with cardId.
      businessNetworkConnection = new BusinessNetworkConnection();
      await businessNetworkConnection.connect(cardId);

      // Get patient from the network.
      const doctorRegistry = await businessNetworkConnection.getParticipantRegistry(participantNamespace + '.Doctor');
      const doctor = await doctorRegistry.get(doctorId);
      console.log("doctor: ", doctor);

      // Disconnect.
      await businessNetworkConnection.disconnect(cardId);

      // Return patient object
      return doctor;
    }
    catch(err) {
      //print and return error
      console.log("Error", err);
      var error = {};
      error.error = err.message;
      return error;
    }
  },
}